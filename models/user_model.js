/**
 * Basic User Schema
 */

const Mongoose = require('mongoose');

const UserSchema = new Mongoose.Schema({
    name: { type: String, trim: true, required: true },
    lastname: { type: String, trim: true, required: true },
    email: { type: String, trim: true },
    active: { type: Boolean, default: true },
    deleted_at: { type: Date, default: null },
}, { collection: 'user', timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = Mongoose.model('User', UserSchema);
