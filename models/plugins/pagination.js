/**
 * @package mongoose-paginate
 * @param {Object} [query={}]
 * @param {Object} [options={}]
 * @param {Object|String} [options.select]
 * @param {Object|String} [options.sort]
 * @param {Array|Object|String} [options.populate]
 * @param {Boolean} [options.lean=false]
 * @param {Boolean} [options.leanWithId=true]
 * @param {Number} [options.offset=0] - Use offset or page to set skip position
 * @param {Number} [options.page=1]
 * @param {Number} [options.limit=10]
 * @param {Function} [callback]
 *
 * @returns {Promise}
 */
function paginate(query = {}, options, callback) {
    const queryOptions = { ...(paginate.options), ...options };
    const { select, sort, populate } = queryOptions;
    const lean = queryOptions.lean || false;
    const leanWithId = queryOptions.leanWithId ? queryOptions.leanWithId : true;
    const limit = queryOptions.limit ? queryOptions.limit : 10;
    const page = queryOptions.page || 1;
    const offset = queryOptions.offset || 0;
    const skip = queryOptions.offset || (queryOptions.page) ? (page - 1) * limit : offset;
    let promises;

    if (limit) {
        const docsQuery = this.find(query)
            .select(select)
            .sort(sort)
            .skip(skip)
            .limit(limit)
            .lean(lean);

        if (populate) {
            [].concat(populate).forEach((item) => {
                docsQuery.populate(item);
            });
        }

        promises = {
            docs: docsQuery.exec(),
            count: this.count(query).exec(),
        };

        if (lean && leanWithId) {
            promises.docs = promises.docs.then((docs) => {
                docs.forEach((doc) => {
                    doc.id = String(doc._id); // eslint-disable-line
                });
                return docs;
            });
        }
    }

    promises = Object.keys(promises).map(x => promises[x]);
    return Promise.all(promises).then((data) => {
        const result = {
            docs: data.docs,
            total: data.count,
            limit,
        };
        if (offset !== undefined) {
            result.offset = offset;
        }
        if (page !== undefined) {
            result.page = page;
            result.pages = Math.ceil(data.count / limit) || 1;
        }
        if (typeof callback === 'function') {
            return callback(null, result);
        }
        const promise = new Promise();
        promise.resolve(result);
        return promise;
    });
}

/**
 * @param {Schema} schema
 */

module.exports = (schema) => {
    schema.statics.paginate = paginate; // eslint-disable-line
};

module.exports.paginate = paginate;
