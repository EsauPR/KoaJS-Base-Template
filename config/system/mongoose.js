const Mongoose = require('mongoose');
const Logger = require('./../../utils/logger');
const fileLoader = require('./../../utils/fileLoader');

Mongoose.Promise = global.Promise;

const logger = new Logger();

/**
 * Load the mongoose models
 * @param {function}    callback    Callback function
 */
function loadModels(callback) {
    const modelsPath = `${__dirname}/../../models`;
    fileLoader(modelsPath, (modelPath) => require(modelPath)); // eslint-disable-line
    if (callback) callback();
}

/**
 * Initialize the connection to the db
 * @param {function}    Callback    Callback function
 */
function connect(callback) {
    const options = {
        promiseLibrary: global.Promise,
        useNewUrlParser: true,
        dbName: process.env.MONGO_DB,
    };
    const db = Mongoose.connect(process.env.MONGO_URI, options, (err) => {
        if (err) {
            logger.warn('Could not connect to MongoDB!');
            logger.error(err);
        } else {
            logger.info('MongoDB connection established');
            // Enabling mongoose debug mode if required
            Mongoose.set('debug', (process.env.MONGO_DEBUG === 'true'));
            // Call callback FN
            if (callback) callback(db);
        }
    });
}

/**
 * Finalize the connection to the db
 * @param {function}    callback    Callback function
 */
function disconnect(callback) {
    Mongoose.disconnect((err) => {
        if (err) {
            logger.warn('Could not disconnect to MongoDB!');
            logger.error(err);
        } else {
            logger.info('Disconnected from MongoDB.');
            if (callback) callback();
        }
    });
}

/**
 * Initialize the connection process
 * @param {function}    callback    Callback function
 */
function init(callback) {
    connect(() => {
        loadModels(() => {
            logger.info('Loaded Models');
            if (callback) callback();
        });
    });
}

module.exports = { init, connect, disconnect };
