#!/bin/bash
if [ "$DOWNLOAD" ]; then
  rm -f ./config/production_variables.env;
  aws s3 cp $DOWNLOAD ./config/production_variables.env;
  node index.js config/production_variables.env
else
  node index.js
fi
