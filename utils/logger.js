const winston = require('winston');

/**
 * Provides a singleton for a logger.
 */
let instance = null;

class Logger {
    constructor(level = null) {
        if (!instance) {
            instance = Logger.createInstance();
        }
        if (level) instance.level = level;
        return instance;
    }

    static customFormat(info) {
        let timestamp = new Date(info.timestamp);
        timestamp = `${timestamp.toLocaleString()} UTC${timestamp.getTimezoneOffset() / -60}`;
        return `${timestamp} ${info.level}: ${info.message}`;
    }

    static createInstance() {
        const loggerInstance = winston.createLogger({
            level: process.env.LOGGER_LEVEL || 'info',
            format: winston.format.combine(
                winston.format.splat(),
                winston.format.colorize(),
                winston.format.timestamp(),
                winston.format.printf(Logger.customFormat),
            ),
            transports: [
                new winston.transports.Console(),
            ],
        });
        return loggerInstance;
    }
}

module.exports = Logger;
