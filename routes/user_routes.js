const UserController = require('../controllers/user_controller');
// const auth = require('../middlewares/auth');

/**
 * Set the routes defined inside
 * @param {object}  router  koa router object
 */
function routeSetter(router) {
    router.get('/v1/users', UserController('getAll'));
    router.get('/v1/users/:userId', UserController('get'));
    router.post('/v1/users/', UserController('create'));
    router.patch('/v1/users/:userId', UserController('update'));
    router.delete('/v1/users/:userId', UserController('delete'));
}

module.exports = routeSetter;
