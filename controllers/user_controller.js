const BaseController = require('../kernel/controllers/base_controller');
const UserModel = require('../models/user_model');


class UserController extends BaseController {
    constructor() {
        super('user', UserModel);
        this.useSoftDelete = false;
    }

    getCreateValidator() {
        return this.Joi.object().keys({
            name: this.Joi.string().required(),
            lastname: this.Joi.string().required(),
            email: this.Joi.string().email().required(),
        });
    }

    getUpdateValidator() {
        return this.Joi.object().keys({
            name: this.Joi.string(),
            lastname: this.Joi.string(),
            email: this.Joi.string().email(),
        });
    }
}


module.exports = UserController.load(new UserController());
