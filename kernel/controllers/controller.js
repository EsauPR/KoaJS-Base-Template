const { ObjectId } = require('mongoose').Types;
const Joi = require('joi');
const { responseSuccess, responseError, httpCodes } = require('../http/responses');
const controllerLoader = require('../factory/controller_loader');
const Logger = require('../../utils/logger');

/**
 * Controller class
 *
 * Provide methods to validate input
 * Provide methods to handle responses
 */
class Controller {
    constructor() {
        // Helpers for Http responses
        this.httpCodes = httpCodes;
        this.responseSuccess = responseSuccess;
        this.responseError = responseError;
        // Library for validations
        this.Joi = Joi;
        // Mongo ObjectId class
        this.ObjectId = ObjectId;
        // Main looger
        this.logger = new Logger();
    }

    /**
     * Provides a compatible interface with Koa Routes to load a class as Controller
     * @param {Object} controller Controller instance
     * @returns {funtion} Function that handle the controller execution
     */
    static load(controller) {
        return controllerLoader(controller);
    }

    /**
     * Set the default permissions to each method
     * @param  {Object} methods Object with the methods to protect in te format
     *                          { method: permission }
     */
    protectMethods(methodsProtected) {
        this.protectedMethods = methodsProtected || {};
    }

    /**
     * Validate the user input data for the incomming request
     * @param  {Object} ctx    Koa context object
     * @param  {Object} schema Joi Schema
     * @return {Object}        Object with user input data validated
     *
     * @throws {Response Error} If validation fails
     *
     * @see https://github.com/hapijs/joi
     */
    validator(ctx, schema) {
        const attributes = ctx.request.body;
        const validation = this.Joi.validate(attributes, schema, {
            convert: false,
        });

        if (validation.error) {
            ctx.logger.debug(validation.error.toString());
            let message = validation.error.toString();
            const index = message.indexOf('[');
            message = message.slice(index + 1, -1);
            const error = new Error(message);
            this.responseError(ctx, this.httpCodes.VALIDATION_ERROR, error);
        }

        return attributes;
    }


    isValidObjectId(id) {
        return this.ObjectId.isValid(id);
    }
}

module.exports = Controller;
