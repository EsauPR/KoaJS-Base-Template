const Controller = require('./controller');

/**
 * Base Controller class
 *
 * The controllers that inherits from BaseController
 * will have the basic CRUD OPERATIONS for any entity
 *
 * @param {string} entity Entity that the controller will represents
 * @param {Object} Model  Model will be used for CRUD operations
 *
 */
class BaseController extends Controller {
    constructor(entity, Model) {
        super();
        // Name for the entity that the controller represents
        // For example 'User'
        this.entity = entity;
        // Main model used into the controller that inherits from BaseController
        // This model will be used for CRUD operations
        this.Model = Model;
        // Valid query filters used to filter the results from DB
        this.validQueryFilters = null;
        // Valid query fields used to return selected information
        this.validQueryFields = null;
        // Valid sort fields used to sort the information
        this.validQuerySort = null;
        // If this is set to false the method delete will remove the element from DB
        // If this is set to true the method delete will use a soft delete way
        this.useSoftDelete = true;
    }


    /**
     * Returns the entity that represents the controller
     */
    getEntity() {
        if (!this.entity) {
            throw new Error('You must especify the controller entity');
        }
        return this.entity;
    }


    /**
     * Returns the main Model for the entoty that represents the controller
     */
    getModel() {
        if (!this.Model) {
            throw new Error('You must especify the controller Model');
        }
        return this.Model;
    }


    /**
     * This method must be implemented into the class that inherit
     * from BaseController, this method must returns a validation schema.
     *
     * The schema validations are defined with JoiJS
     *
     * The schema returned is used to validate the input for the method created
     *
     * @param {ctx} ctx Koa context
     *
     * @returns {Object} Schema Validations
     *
     * @see https://github.com/hapijs/joi
     */
    getCreateValidator(ctx) { // eslint-disable-line
        throw new Error('The method "getCreateValidator" must be implemented');
    }


    /**
     * This method must be implemented into the class that inherit
     * from BaseController, this method must returns a validation schema.
     *
     * The schema validations are defined with JoiJS
     *
     * @param {ctx} ctx Koa context
     *
     * @returns {Object} Schema Validations
     *
     * @see https://github.com/hapijs/joi
     */
    getUpdateValidator(ctx) { // eslint-disable-line
        throw new Error('The method "getUpdateValidator" must be implemented');
    }


    /**
     * Validate a ObjectId
     * @param {Object} ctx koa context
     * @param {String} label Name for the ObjectId
     * @param {String} value Value to validate as ObjectId
     */
    validateObjectId(ctx, label, value) {
        const message = `Invalid id "${label}" for "${value}"`;
        if (!this.isValidObjectId(value)) {
            this.responseError(ctx, this.httpCodes.VALIDATION_ERROR, message);
        }
    }


    /**
     * Generic inplementation to get any entity
     *
     * @param {Object} ctx Koa context
     */
    async get(ctx) {
        const Model = this.getModel();
        const entityLabel = `${this.getEntity()}Id`;
        const entityId = ctx.params[entityLabel];

        this.validateObjectId(ctx, entityLabel, entityId);

        const result = await Model.findOne({
            _id: entityId,
            $or: [
                { deleted_at: null },
                { deleted_at: { $exists: false } },
            ],
        }).catch((error) => {
            this.responseError(ctx, this.httpCodes.DB_ERROR, error);
        });

        if (!result) {
            const message = `${Model.modelName} with id ${entityId} not found`;
            this.responseError(ctx, this.httpCodes.NOT_FOUND_ERROR, message);
        }

        return this.responseSuccess(ctx, {
            data: result,
        });
    }


    /**
     * Generic inplementation to get all the entities
     *
     * @param {Object} ctx Koa context
     */
    async getAll(ctx) {
        const Model = this.getModel();
        const results = await Model.find({
            $or: [
                { deleted_at: null },
                { deleted_at: { $exists: false } },
            ],
        }).catch((error) => {
            this.responseError(ctx, this.httpCodes.DB_ERROR, error);
        });

        return this.responseSuccess(ctx, {
            data: results,
        });
    }


    /**
     * Generic inplementation to create a entity
     *
     * @param {Object} ctx Koa context
     */
    async create(ctx) {
        const Model = this.getModel();
        const data = this.validator(ctx, this.getCreateValidator());
        let result = new Model(data);

        result = await result.save().catch((error) => {
            this.responseError(ctx, this.httpCodes.DB_ERROR, error);
        });

        return this.responseSuccess(ctx, {
            data: result,
        });
    }


    /**
     * Generic inplementation to update a entity
     *
     * @param {Object} ctx Koa context
     */
    async update(ctx) {
        const Model = this.getModel();
        const entityLabel = `${this.getEntity()}Id`;
        const entityId = ctx.params[entityLabel];

        this.validateObjectId(ctx, entityLabel, entityId);
        const data = this.validator(ctx, this.getUpdateValidator());

        let result = new Model(data);

        result = await Model.findByIdAndUpdate(entityId, { $set: data }, { new: true })
            .catch((error) => {
                this.responseError(ctx, this.httpCodes.DB_ERROR, error);
            });

        if (!result) {
            const message = `${Model.modelName} with id ${entityId} not found`;
            this.responseError(ctx, this.httpCodes.NOT_FOUND_ERROR, message);
        }

        return this.responseSuccess(ctx, {
            data: result,
        });
    }


    /**
     * Delete a entity using soft delete method
     *
     * @param {Object} ctx Koa context
     */
    async _softDelete(ctx, entityId) {
        const Model = this.getModel();
        const result = await Model.findByIdAndUpdate(
            entityId,
            { $set: { deleted_at: new Date() } },
            { new: true },
        ).catch((error) => {
            this.responseError(ctx, this.httpCodes.NOT_FOUND_ERROR, error);
        });

        return result;
    }


    /**
     * Delete a entity from the DB
     *
     * @param {Object} ctx Koa context
     */
    async _hardDelete(ctx, entityId) {
        const Model = this.getModel();
        const result = await Model.findByIdAndRemove(entityId).catch((error) => {
            this.responseError(ctx, this.httpCodes.NOT_FOUND_ERROR, error);
        });

        return result;
    }


    /**
     * Generic inplementation to delete a entity
     *
     * @param {Object} ctx Koa context
     */
    async delete(ctx) {
        const Model = this.getModel();
        const entityLabel = `${this.getEntity()}Id`;
        const entityId = ctx.params[entityLabel];

        let result = null;
        if (this.useSoftDelete) {
            result = await this._softDelete(ctx, entityId);
        } else {
            result = await this._hardDelete(ctx, entityId);
        }

        if (!result) {
            const message = `${Model.modelName} with id ${entityId} not found`;
            this.responseError(ctx, this.httpCodes.NOT_FOUND_ERROR, message);
        }

        return this.responseSuccess(ctx, {
            status: 204,
        });
    }
}


module.exports = BaseController;
