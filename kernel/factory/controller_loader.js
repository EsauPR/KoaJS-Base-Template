const { responseError, httpCodes } = require('../http/responses');

/**
 * Privide a factory to create a new controller used for koa router
 *
 * This is necessary cause you can not access to a method from the route specification
 * because the scope of accessing to methods with *this*
 *
 * So the solcution is that you can tell with method execute from the controller using
 * this factory
 *
 * @param  {object} controller Controller instance
 * @return {function}          Method to execute
 *
 * @throws {Response Error} There are not permissions to access to the method
 */
function controllerLoader(controller) {
    return method => (ctx) => {
        const { protectedMethods } = controller;
        if (protectedMethods) {
            const permissionRequired = protectedMethods[method];
            if (permissionRequired) {
                const permissionsAuthorized = ctx.state.user.permissions || [];
                if (!permissionsAuthorized.find(permission => permission === permissionRequired)) {
                    ctx.logger.debug(`Permission required: ${permissionRequired}`);
                    ctx.logger.debug('Permissions autorized:', permissionsAuthorized);
                    responseError(ctx, httpCodes.FORBIDDEN_ERROR, 'Permissions required not found');
                }
            }
        }
        if (!controller[method]) {
            throw new Error(`Controller for "${controller.entity}" has not method "${method}"`);
        }
        return (controller[method])(ctx);
    };
}

module.exports = controllerLoader;
