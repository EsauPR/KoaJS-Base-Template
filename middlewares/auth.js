const jwt = require('jsonwebtoken');
const User = require('../models/user_model');
const { httpCodes, responseError } = require('../kernel/http/responses');

/**
 * Middlewares for jwt authentication
 *
 * @param   {object}    ctx     Koa context object
 * @param   {function}  next    Next middleware
 */
async function auth(ctx, next) {
    // Authentication
    const { authorization } = ctx.headers;
    if (!authorization) {
        responseError(ctx, httpCodes.UNAUTHORIZED_ERROR, 'Access token not Found');
    }
    // Check if is a valid token type
    const [bearer, accessToken] = authorization.trim().split(' ');
    if (bearer !== 'Bearer') {
        responseError(ctx, httpCodes.UNAUTHORIZED_ERROR, 'Token must be a bearer type');
    }
    // Decode the token
    let decoded;
    try {
        decoded = jwt.verify(accessToken, process.env.APP_TOKEN_SECRET);
    } catch (error) {
        responseError(ctx, httpCodes.UNAUTHORIZED_ERROR, error.message);
    }
    // finde the user and Attach the user information into the context
    try {
        const user = await User.findOne({ kgroup_user_id: decoded.sub });
        if (!user) {
            responseError(ctx, httpCodes.UNAUTHORIZED_ERROR, 'User not Found');
        }
        ctx.state.user = JSON.parse(JSON.stringify(user));
    } catch (error) {
        responseError(ctx, httpCodes.DB_ERROR, error);
    }
    // Attach the Authorization to the context
    ctx.state.user.permissions = decoded.permissions || [];

    await next();
}

module.exports = auth;
